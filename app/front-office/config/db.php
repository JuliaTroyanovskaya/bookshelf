<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql;port=3306;dbname=library',
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8',
];
