<?php

namespace App;

//use App\Interfaces\IUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Model
{
    use Notifiable;

    protected $table = 'users';

    public $timestamps = false;

    protected $appends = ['email', 'main'];

    protected $fillable = ['first_name', 'last_name', 'admin'];

    public function emails()
    {
        return $this->hasMany('\App\Email');
    }

    public function getEmailAttribute()
    {
        return $this->emails()
            ->where('user_id', '=', $this->id)
            ->where('main', '=', true)
            ->first()
            ->email;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];
}
