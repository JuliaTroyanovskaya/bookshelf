<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = [
        'Title',
        'ISBN',
        'Description',
        'Format',
        'PagesCount',
        'YearOfPublication'];

    //
}
