<?php

namespace App\Providers;

use App\Services\MailService;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('MailService',function (){
            return new MailService();
        });
//        $this->app->singleton('AppMailService',function (){
//            return new AppMailService();
//        });
    }
}
