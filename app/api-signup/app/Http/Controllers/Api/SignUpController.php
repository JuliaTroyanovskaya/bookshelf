<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 22.07.17
 * Time: 12:00
 */

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use Validator;
use App\Models\User;
use App\Services\MailService;
use GuzzleHttp\Client;

class SignUpController extends Controller
{
    public function signUpUser(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'first_name',
            'last_name',
            'password'
        );

        $validationParams = [
            'email' => 'required|email|unique:emails,email',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationParams
        );

        if ($validator->fails()) {

            return (new App\Responses\JSONAPIResponse())
                ->setErrors($validator->errors())
                ->setStatusCode(400);

        }

        $userParams = [
            'first_name' => $requestParams['first_name'],
            'last_name' => $requestParams['last_name'],
            'admin' => 0
        ];

        $user = new User($userParams);
        $user->save();

        $email = $user->emails()->create([
            'user_id' => $user->id,
            'email' => $requestParams['email'],
            'main' => true
        ]);

        $email->password()->create([
            'email_id' => $email->id,
            'password' => md5($requestParams['password'])
        ]);

        app('MailService')->sendRegistrationMail($email);

        return response('User registered', 200);
    }
}
