<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "emails";
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $primaryKey = 'ID';
    public $incrementing = true;
    protected $fillable = [
        'email',
        'main',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function password()
    {
        return $this->hasOne('App\Password');
    }
}
