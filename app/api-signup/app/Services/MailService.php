<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 24.07.17
 * Time: 16:43
 */

namespace App\Services;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MailService
{
    /**
     *  Отправляет запрос на отправку регистрационного
     * письма новому пользователю.
     *
     * @param $userId
     * @return bool
     */
    public function sendRegistrationMail($email)
    {
        $messageParams = [
            'mail_id' => REGISTRATION_MAIL,
            'email' => $email->email
        ];

        $connection = new AMQPStreamConnection('RABBIT_HOST',
            RABBIT_PORT, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare('hello', false, false, false, false);

        $msg = new AMQPMessage(json_encode($messageParams));
        $channel->basic_publish($msg, '', 'sendMail');

        $channel->close();
        $connection->close();
    }
}