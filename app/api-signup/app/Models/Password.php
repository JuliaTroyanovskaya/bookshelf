<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = "passwords";
    public $timestamps = false;
    protected $fillable = [
        'password',
        'email_id'
    ];

}
