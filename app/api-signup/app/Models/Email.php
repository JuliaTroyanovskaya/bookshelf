<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "emails";
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = [
        'email',
        'main',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function password()
    {
        return $this->hasOne('App\Models\Password');
    }
}
