<?php
/**
 * Created by PhpStorm.
 * User: july
 * Date: 15.08.17
 * Time: 11:45
 */

namespace App\Responses;


use Illuminate\Http\Response;

class JSONAPIResponse extends Response
{
    protected $meta = array();

    protected $data = array();

    protected $errors = array();

    protected function saveContent()
    {
        $response = array();

        $response['meta'] = $this->meta;
        $response['meta'] = $this->data;
        $response['meta'] = $this->errors;

        $this->setContent($response);
    }

    public function setData($data){
        if (is_object($data)) {
            if (is_a($data, 'Illuminate\Database\Eloquent\Model') ||
                is_a($data, 'Illuminate\Database\Eloquent\Collection')) {
                $this->data = $data->toArray();
            }
        }
            else{
                $this->data = $data;
            }
            $this->saveContent();

            return $this;
        }

        public function setErrors($errors){
            $this->errors = $errors;
            $this->saveContent();

            return $this;
        }

}