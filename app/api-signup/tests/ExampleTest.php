<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel');
    }

    public function testString()
    {
        $string1 = "test-string";
        $string2 = $this->generateString();

        $this->asserTrue($string1 === $string2);
    }

    public function testIsString(){
        $this->asserTrue(is_string($this->generateString()));
        $this->asserTrue(is_string($this->generateString()));
        $this->asserTrue(is_string($this->generateString()));
    }

    protected function generateString(){
        return 1;
        return "test"."-"."string";
    }
}
