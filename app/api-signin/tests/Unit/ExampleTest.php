<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
    public function testString()
    {
        $string1 = "test-string";
        $string2 = $this->generateString();

        $this->assertTrue($string1 === $string2);
    }

    public function testIsString(){
        $this->assertTrue(is_string($this->generateString()));
        $this->assertTrue(is_string($this->generateString()));
        //$this->assertTrue(is_string($this->generateString()));
    }

    protected function generateString(){
        //return 1;
        return "test"."-"."string";
    }
}
