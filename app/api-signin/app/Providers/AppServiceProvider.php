<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Memcached;
use Config;
use App\Models\UserSession;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cache',function (){
            $object = new Memcached();
            $object->addServers(Config::get('cache.memcacheed.servers'));

            return $object;
        });

        $this->app->singleton('userSession', function($app){

            $session = app('request')->header('session_id');

            $cache = app('cache');

            $sessionInfo = $cache->get($session);
            if (!$sessionInfo){
                return null;
            }

            return new UserSession($sessionInfo);


        });
    }
}
