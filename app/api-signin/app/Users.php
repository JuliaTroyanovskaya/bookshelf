<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    protected $table = "users";
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $primaryKey = 'ID';
    public $incrementing = true;
    protected $fillable = [
        'FirstName',
        'LastName'
    ];
}
