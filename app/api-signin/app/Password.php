<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = "passwords";
    public $timestamps = false;
    protected $fillable = [
        'Password',
        'Email_ID'
    ];
}
