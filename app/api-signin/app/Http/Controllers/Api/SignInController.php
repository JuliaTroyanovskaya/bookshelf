<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;


class SignInController extends Controller
{
    /**
     * @email string
     * @passwor string
     *
     *
     * @responce = {
        "session":$hash,
        "expire":$time,
        "first_name":$firstName,
        "last_name":$lastName
     * }
     * @param Request $request
     */

      public function auth(Request $request)
    {

        $requestParams = $request->only(
            'login',
            'password'
        );

        $cache = app('cache');

        $hasToken = $cache->get($requestParams['login']);
        if ($hasToken){
            return response('User are logged', 403);
        }

        $cachedUser = $cache->get(md5($requestParams['login'].$requestParams['password']));
        if(is_array($cachedUser)){
            $user = User::find($cachedUser['id']);
        }
        else{
            $user = User::join('emails','emails.user_id','=','users.id')
                ->join('passwords', 'passwords.email_id', '=', 'email_id')
                ->where('emails.email', '=', $requestParams['login'])
                ->where('passwords.password', '=', $requestParams['password'])
                ->select('users.*', 'emails.email')
                ->first();
        }
        if($user){

        }
        return response('Bad email or pass', 400);
        //
    }


    public function checkSession(Request $request)
    {
        //
        return "CheckSession";
    }

    public function expire(Request $request)
    {
        //
        return "expireSession";
    }

    /**
     * @session
     *
     */
    public function logout(Request $request)
    {
        //
    }

    public function set_password($by_token=null){
        return $by_token;
    }

    public function request_password_reset(){
        return 'request_password_reset';
    }


}
