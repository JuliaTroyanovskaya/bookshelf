<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function checkSession(Request $request)
    {
//        $session = $request->header('session_id');
//
//        $cache = app('cache');
//
//        $sessionInfo = $cache->get($session);

        $request->header();

        $sessionInfo = app('userSession');

        if(!$sessionInfo ){
            return response('Session expired', 403);
        }


        if($sessionInfo['expire'] <
            time() + Config::get('session.lifetime')*30)
        {
            $sessionInfo->prolong();
//            $expire = Config::get('session.lifetime')*60;
//            $sessionInfo['expire'] = $expire;
//            $cache->set($session, $sessionInfo, $expire);
        }

        return response('Good session', 200);
    }
}
