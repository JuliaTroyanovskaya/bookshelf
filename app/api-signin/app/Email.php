<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "emails";
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $primaryKey = 'ID';
    public $incrementing = true;
    protected $fillable = [
        'Email',
        'IsMain',
        'User_ID'
    ];
}
