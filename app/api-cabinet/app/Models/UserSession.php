<?php
/**
 * Created by PhpStorm.
 * User: july
 * Date: 08.08.17
 * Time: 21:04
 */

namespace App\Models;


class UserSession
{
    protected $userId;

    protected $expire;

    public function __construct($params)
    {
        $this->userId = $params['id'];
        $this->expire = $params['expire'];
    }

    public function user()
    {
        return User::find($this->userId);
    }

    public function getExpiresAt(){
        return $this->expire;
    }

    public function prolong()
    {
        $expire = Config::get('session.lifetime')*60;
        app('cache')->set(md5($this->userId), [
            'id' => $this->userId,
            'expire' => $expire
        ], $expire);

        $this->expire = $expire;
    }
}