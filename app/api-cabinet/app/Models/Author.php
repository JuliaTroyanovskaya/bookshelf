<?php
/**
 * Created by PhpStorm.
 * User: july
 * Date: 08.08.17
 * Time: 20:48
 */

namespace App;


class Author extends Model
{
    public function books(){
        return $this->hasMany('\App\Models\Book');
    }

    public function  getBooks(){
        return $this->books();
//       return $this->join('author_id', '=', $this->id)
//             ->get();
    }
}