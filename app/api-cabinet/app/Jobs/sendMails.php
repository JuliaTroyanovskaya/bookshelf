<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
//use Illuminate\Support\Facades\Mail;



$connection = new AMQPStreamConnection(env('RABBIT_HOST'),env('RABBIT_PORT'), 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('sendMail', false, false, false, false);

$callback = function($msg) {
    $messageParams = json_encode($msg->body);

    switch ($messageParams['mail_id']){
        case REGISTRATION_MAIL:
            sendRegistrationMail($messageParams['email']);
            break;
    }
    //$view = view('registration.view', ['token' => md5($messageParams['email'])]);

    //mail($messageParams['email'], 'Ragistration library', $view);

};

$channel->basic_consume('sendMail', '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

function sendRegistrationMail($to)
{

    $view = view('registration.view', ['token' => md5($to)]);

    mail($to['email'], 'Ragistration library', $view);
}

$channel->close();
$connection->close();
