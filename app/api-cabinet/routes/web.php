<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo "Hello, World!";
    return;
});

Route::get('restore/password/{token}/section/{?param}', 'Controller@method' )
    ->where('token','[a-z0-9]{36,}')
    ->where('param','[0-9]*');

